var request = require('request');
var Q = require('q');

var api_url = process.env.GOOGLE_API;

(function(){
    var googleApi = function () {
        this.bookLookup = function(query, params) {
            var deferred  = Q.defer();
            if (params.author && params.title) {
                var url = api_url + params.title + '+inauthor:=' + params.author;
            } else if (params.author) {
                var url = api_url + query.replace(params.author, '').replace('by', '') + '+inauthor:=' + params.author;
            } else {
                var url = api_url + query;
            }
            var dataModel = this.dataModel;
            dataModel.getCache(url).then((result) => {
                deferred.resolve(result);
            }, (reason) => {
                request({
                    url: url,
                    method: 'GET'
                }, function (err, res) {
                    if (!err && res.statusCode == 200) {
                        var body = JSON.parse(res.body);
                        if (body.totalItems > 0) {
                            var books = body.items.map((vol) => {
                                return {
                                    title: (vol.volumeInfo.title + (vol.volumeInfo.subtitle ? ' ' + vol.volumeInfo.subtitle : '')).replace(/\s*\(.*?\)\s*/g, ' '),
                                    author: vol.volumeInfo.authors ? vol.volumeInfo.authors[0] : null,
                                    publisher: vol.volumeInfo.publisher,
                                    isbn: (vol.volumeInfo.industryIdentifiers && vol.volumeInfo.industryIdentifiers == 2) ? vol.volumeInfo.industryIdentifiers.filter((identifier) => {
                                        return (identifier.type == 'ISBN_13');
                                    })[0].identifier : '',
                                    thumbnail: vol.volumeInfo.imageLinks ? vol.volumeInfo.imageLinks.thumbnail : ''
                                };
                            });
                            dataModel.setCache(url, books);
                        } else {
                            var books = [];
                        }
                        deferred.resolve(books);
                    } else {
                        deferred.reject(err);
                    }
                });
            });

            return deferred.promise;
        };
    };

    module.exports = googleApi;
})();
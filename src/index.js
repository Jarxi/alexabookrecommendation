/**
    Copyright 2014-2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.

    Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at

        http://aws.amazon.com/apache2.0/

    or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
*/

/**
 * This sample shows how to create a Lambda function for handling Alexa Skill requests that:
 * - Web service: communicate with an external web service to get tide data from NOAA CO-OPS API (http://tidesandcurrents.noaa.gov/api/)
 * - Multiple optional slots: has 2 slots (city and date), where the user can provide 0, 1, or 2 values, and assumes defaults for the unprovided values
 * - DATE slot: demonstrates date handling and formatted date responses appropriate for speech
 * - Custom slot type: demonstrates using custom slot types to handle a finite set of known values
 * - Dialog and Session state: Handles two models, both a one-shot ask and tell model, and a multi-turn dialog model.
 *   If the user provides an incorrect slot in a one-shot model, it will direct to the dialog model. See the
 *   examples section for sample interactions of these models.
 * - Pre-recorded audio: Uses the SSML 'audio' tag to include an ocean wave sound in the welcome response.
 *
 * Examples:
 * One-shot model:
 *  User:  "Alexa, ask Book Pooler when is the high tide in Seattle on Saturday"
 *  Alexa: "Saturday June 20th in Seattle the first high tide will be around 7:18 am,
 *          and will peak at ...""
 * Dialog model:
 *  User:  "Alexa, open Book Pooler"
 *  Alexa: "Welcome to Book Pooler. Which city would you like tide information for?"
 *  User:  "Seattle"
 *  Alexa: "For which date?"
 *  User:  "this Saturday"
 *  Alexa: "Saturday June 20th in Seattle the first high tide will be around 7:18 am,
 *          and will peak at ...""
 */

/**
 * App ID for the skill
 */
var APP_ID = "amzn1.ask.skill.89dda57a-caad-4215-aa9b-3680a4f777c5";//replace with 'amzn1.echo-sdk-ams.app.[your-unique-value-here]';

var http = require('http'),
    alexaDateUtil = require('./alexaDateUtil');

/**
 * The AlexaSkill prototype and helper functions
 */
var AlexaSkill = require('./AlexaSkill');

var books = require('google-books-search');
var striptags = require('striptags');

/**
 * BookRecommendation is a child of AlexaSkill.
 * To read more about inheritance in JavaScript, see the link below.
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Introduction_to_Object-Oriented_JavaScript#Inheritance
 */
var BookRecommendation = function () {
    AlexaSkill.call(this, APP_ID);
};

// Extend AlexaSkill
BookRecommendation.prototype = Object.create(AlexaSkill.prototype);
BookRecommendation.prototype.constructor = BookRecommendation;

// ----------------------- Override AlexaSkill request and intent handlers -----------------------

BookRecommendation.prototype.eventHandlers.onSessionStarted = function (sessionStartedRequest, session) {
    console.log("onSessionStarted requestId: " + sessionStartedRequest.requestId
        + ", sessionId: " + session.sessionId);
    // any initialization logic goes here
};

BookRecommendation.prototype.eventHandlers.onLaunch = function (launchRequest, session, response) {
    console.log("onLaunch requestId: " + launchRequest.requestId + ", sessionId: " + session.sessionId);
    handleWelcomeRequest(response);
};

BookRecommendation.prototype.eventHandlers.onSessionEnded = function (sessionEndedRequest, session) {
    console.log("onSessionEnded requestId: " + sessionEndedRequest.requestId
        + ", sessionId: " + session.sessionId);
    // any cleanup logic goes here
};

/**
 * override intentHandlers to map intent handling functions.
 */
BookRecommendation.prototype.intentHandlers = {
    "OneshotBookIntent": function (intent, session, response) {
        handleOneshotBookRequest(intent, session, response);
    },

    "DialogBookIntent": function (intent, session, response) {
        // Determine if this turn is for city, for date, or an error.
        // We could be passed slots with values, no slots, slots with no value.
        var citySlot = intent.slots.City;
        var dateSlot = intent.slots.Date;
        if (citySlot && citySlot.value) {
            handleCityDialogRequest(intent, session, response);
        } else if (dateSlot && dateSlot.value) {
            handleDateDialogRequest(intent, session, response);
        } else {
            handleNoSlotDialogRequest(intent, session, response);
        }
    },

    "SupportedCitiesIntent": function (intent, session, response) {
        handleSupportedCitiesRequest(intent, session, response);
    },

    "AMAZON.HelpIntent": function (intent, session, response) {
        handleHelpRequest(response);
    },

    "AMAZON.StopIntent": function (intent, session, response) {
        var speechOutput = "Goodbye";
        response.tell(speechOutput);
    },

    "AMAZON.CancelIntent": function (intent, session, response) {
        var speechOutput = "Goodbye";
        response.tell(speechOutput);
    }
};

// -------------------------- BookRecommendation Domain Specific Business Logic --------------------------

// example city to NOAA station mapping. Can be found on: http://tidesandcurrents.noaa.gov/map/
var STATIONS = {
    'seattle': 9447130,
    'san francisco': 9414290,
    'monterey': 9413450,
    'los angeles': 9410660,
    'san diego': 9410170,
    'boston': 8443970,
    'new york': 8518750,
    'virginia beach': 8638863,
    'wilmington': 8658163,
    'charleston': 8665530,
    'beaufort': 8656483,
    'myrtle beach': 8661070,
    'miami': 8723214,
    'tampa': 8726667,
    'new orleans': 8761927,
    'galveston': 8771341
};

var IMPRINTS = {
    0: 'AvonRomance',
    1: 'HarperOne',
    2: 'HarperCollins 360'
};

function handleWelcomeRequest(response) {
    var whichCityPrompt = "Which city would you like tide information for?",
        speechOutput = {
            speech: "<speak>Welcome to Book Pooler. "
                + "<audio src='https://s3.amazonaws.com/ask-storage/BookRecommendation/OceanWaves.mp3'/>"
                + whichCityPrompt
                + "</speak>",
            type: AlexaSkill.speechOutputType.SSML
        },
        repromptOutput = {
            speech: "I can lead you through providing a city and "
                + "day of the week to get tide information, "
                + "or you can simply open Book Pooler and ask a question like, "
                + "get tide information for Seattle on Saturday. "
                + "For a list of supported cities, ask what cities are supported. "
                + whichCityPrompt,
            type: AlexaSkill.speechOutputType.PLAIN_TEXT
        };

    response.ask(speechOutput, repromptOutput);
}

function handleHelpRequest(response) {
    var repromptText = "Which city would you like tide information for?";
    var speechOutput = "I can lead you through providing a city and "
        + "day of the week to get tide information, "
        + "or you can simply open Book Pooler and ask a question like, "
        + "get tide information for Seattle on Saturday. "
        + "For a list of supported cities, ask what cities are supported. "
        + "Or you can say exit. "
        + repromptText;

    response.ask(speechOutput, repromptText);
}

/**
 * Handles the case where the user asked or for, or is otherwise being with supported cities
 */
function handleSupportedCitiesRequest(intent, session, response) {
    // get city re-prompt
    var repromptText = "Which city would you like tide information for?";
    var speechOutput = "Currently, I know tide information for these coastal cities: " + getAllStationsText()
        + repromptText;

    response.ask(speechOutput, repromptText);
}

/**
 * Handles the dialog step where the user provides a city
 */
function handleCityDialogRequest(intent, session, response) {

    var cityStation = getCityStationFromIntent(intent, false),
        repromptText,
        speechOutput;
    if (cityStation.error) {
        repromptText = "Currently, I know tide information for these coastal cities: " + getAllStationsText()
            + "Which city would you like tide information for?";
        // if we received a value for the incorrect city, repeat it to the user, otherwise we received an empty slot
        speechOutput = cityStation.city ? "I'm sorry, I don't have any data for " + cityStation.city + ". " + repromptText : repromptText;
        response.ask(speechOutput, repromptText);
        return;
    }

    // if we don't have a date yet, go to date. If we have a date, we perform the final request
    if (session.attributes.date) {
        getFinalBookResponse(cityStation, session.attributes.date, response);
    } else {
        // set city in session and prompt for date
        session.attributes.city = cityStation;
        speechOutput = "For which date?";
        repromptText = "For which date would you like tide information for " + cityStation.city + "?";

        response.ask(speechOutput, repromptText);
    }
}

/**
 * Handles the dialog step where the user provides a date
 */
function handleDateDialogRequest(intent, session, response) {

    var date = getDateFromIntent(intent),
        repromptText,
        speechOutput;
    if (!date) {
        repromptText = "Please try again saying a day of the week, for example, Saturday. "
            + "For which date would you like tide information?";
        speechOutput = "I'm sorry, I didn't understand that date. " + repromptText;

        response.ask(speechOutput, repromptText);
        return;
    }

    // if we don't have a city yet, go to city. If we have a city, we perform the final request
    if (session.attributes.city) {
        getFinalBookResponse(session.attributes.city, date, response);
    } else {
        // The user provided a date out of turn. Set date in session and prompt for city
        session.attributes.date = date;
        speechOutput = "For which city would you like tide information for " + date.displayDate + "?";
        repromptText = "For which city?";

        response.ask(speechOutput, repromptText);
    }
}

/**
 * Handle no slots, or slot(s) with no values.
 * In the case of a dialog based skill with multiple slots,
 * when passed a slot with no value, we cannot have confidence
 * it is the correct slot type so we rely on session state to
 * determine the next turn in the dialog, and reprompt.
 */
function handleNoSlotDialogRequest(intent, session, response) {
    if (session.attributes.city) {
        // get date re-prompt
        var repromptText = "Please try again saying a day of the week, for example, Saturday. ";
        var speechOutput = repromptText;

        response.ask(speechOutput, repromptText);
    } else {
        // get city re-prompt
        handleSupportedCitiesRequest(intent, session, response);
    }
}

/**
 * This handles the one-shot interaction, where the user utters a phrase like:
 * 'Alexa, open Book Pooler and get tide information for Seattle on Saturday'.
 * If there is an error in a slot, this will guide the user to the dialog approach.
 */
function handleBookRecommendationRequest(intent, session, response) {

    // Determine city, using default if none provided
    var cityStation = getCityStationFromIntent(intent, true),
        repromptText,
        speechOutput;
    if (cityStation.error) {
        // invalid city. move to the dialog
        repromptText = "Currently, I know tide information for these coastal cities: " + getAllStationsText()
            + "Which city would you like tide information for?";
        // if we received a value for the incorrect city, repeat it to the user, otherwise we received an empty slot
        speechOutput = cityStation.city ? "I'm sorry, I don't have any data for " + cityStation.city + ". " + repromptText : repromptText;

        response.ask(speechOutput, repromptText);
        return;
    }

    // Determine custom date
    var date = getDateFromIntent(intent);
    if (!date) {
        // Invalid date. set city in session and prompt for date
        session.attributes.city = cityStation;
        repromptText = "Please try again saying a day of the week, for example, Saturday. "
            + "For which date would you like tide information?";
        speechOutput = "I'm sorry, I didn't understand that date. " + repromptText;

        response.ask(speechOutput, repromptText);
        return;
    }

    // all slots filled, either from the user or by default values. Move to final request
    getFinalBookResponse(cityStation, date, response);
}

function handleOneshotBookRequest(intent, session, response) {

    // Determine city, using default if none provided
    console.dir(intent);
    var Imprint = getImprintFromIntent(intent, true),
        repromptText,
        speechOutput;
    
    console.dir(Imprint);

    if (Imprint.error) {
        // invalid city. move to the dialog
        repromptText = "Currently, I have these publishers to choose from: " + getAllImprintsText()
            + "Which publisher would you like a book recommendation for?";
        // if we received a value for the incorrect city, repeat it to the user, otherwise we received an empty slot
        speechOutput = Imprint.imprint ? "I'm sorry, I don't have a book for " + Imprint.imprint + ". " + repromptText : repromptText;

        response.ask(speechOutput, repromptText);
        return;
    }

    // all slots filled, either from the user or by default values. Move to final request
    getFinalBookResponse(Imprint, response);
}

/**
 * Both the one-shot and dialog based paths lead to this method to issue the request, and
 * respond to the user with the final answer.
 */
function getFinalBookResponse(cityStation, date, response) {

    // Issue the request, and respond to the user
    makeBookRequest(cityStation.station, date, function tideResponseCallback(err, highBookResponse) {
        var speechOutput;

        speechOutput = highBookResponse;

        /*if (err) {
            speechOutput = "Sorry, the National Oceanic tide service is experiencing a problem. Please try again later";
        } else {
            speechOutput = date.displayDate + " in " + cityStation.city + ", the first high tide will be around "
                + highBookResponse.firstHighBookTime + ", and will peak at about " + highBookResponse.firstHighBookHeight
                + ", followed by a low tide at around " + highBookResponse.lowBookTime
                + " that will be about " + highBookResponse.lowBookHeight
                + ". The second high tide will be around " + highBookResponse.secondHighBookTime
                + ", and will peak at about " + highBookResponse.secondHighBookHeight + ".";
        }*/

        response.tellWithCard(speechOutput, "BookRecommendation", speechOutput)
    });
}

function getFinalBookResponse(Imprint, response) {

    // Issue the request, and respond to the user
    console.dir(Imprint);
    makeBookRequest(Imprint.imprint, function bookResponseCallback(err, bookResponse) {
        var speechOutput;

        speechOutput = bookResponse;

        if (err) {
            speechOutput = "Sorry, the Harper Collins book recommendation service is experiencing a problem. Please try again later";
        }

        response.tellWithCard(speechOutput, "BookRecommendation", speechOutput)
    });
}

/**
 * Uses NOAA.gov API, documented: http://tidesandcurrents.noaa.gov/api/
 * Results can be verified at: http://tidesandcurrents.noaa.gov/noaatidepredictions/NOAABooksFacade.jsp?Stationid=[id]
 */
function makeBookRequest(station, date, tideResponseCallback) {

    var datum = "MLLW";
    var endpoint = 'http://api.harpercollins.com/titleservice/v1/products?catalog=AvonRomance&page=1&api_key=6t8fg4vxxcjxm3hhxuccq4fc&sig=2a97ec69cb0aa0c5bd742c95d6e20c67f4ebaccc4043a00384796a23c8468c9b';
    var queryString = '?' + date.requestDateParam;
    queryString += '&station=' + station;
    queryString += '&product=predictions&datum=' + datum + '&units=english&time_zone=lst_ldt&format=json';
    queryString = '';
    var book = 0;
    var title = 'Some title.';
    var author = 'Some author.';
    var noaaResponseString = '';

    http.get(endpoint + queryString, function (res) {

        books.search('Professional JavaScript for Web Developers', function(error, results) {
            if ( ! error ) {
                //console.log("results");
            } else {
                console.log(error);
            }
        });

        
        console.log('Status Code: ' + res.statusCode);

        if (res.statusCode != 200) {
            tideResponseCallback(new Error("Non 200 Response"));
        }

        res.on('data', function (data) {
            noaaResponseString += data;
        });

        res.on('end', function () {
            
            var noaaResponseObject = JSON.parse(noaaResponseString);
            console.log(noaaResponseObject.data[0]);
            
            book = noaaResponseObject.data[0];
            title = book.title;
            author = book.contributors[0].name;
            var keynote = striptags(book.keynote);

            tideResponseCallback(null, "How about " + title + " by " + author + '<break time="1s"/>' + keynote);
        });
    }).on('error', function (e) {
        console.log("Communications error: " + e.message);
        tideResponseCallback(new Error(e.message));
    });
}

function makeBookRequest(imprint, bookResponseCallback) {

    var catalog = imprint;
    var endpoint = 'http://api.harpercollins.com/titleservice/v1/products?catalog=' + catalog + '&page=1&api_key=6t8fg4vxxcjxm3hhxuccq4fc&sig=2a97ec69cb0aa0c5bd742c95d6e20c67f4ebaccc4043a00384796a23c8468c9b';
    var book, title, author;
    var bookResponseString = '';
    
    console.log('endpoint:'+endpoint);

    var rand = function randomIntFromInterval(min, max) {
            return Math.floor(Math.random()*(max-min+1)+min);
        }


    http.get(endpoint, function (res) {

        books.search('Professional JavaScript for Web Developers', function(error, results) {
            if ( ! error ) {
                //console.log("results");
            } else {
                console.log(error);
            }
        });

        console.log('Status Code: ' + res.statusCode);

        if (res.statusCode != 200) {
            bookResponseCallback(new Error("Non 200 Response"));
        }

        res.on('data', function (data) {
            bookResponseString += data;
        });

        res.on('end', function () {
            var bookResponseObject = JSON.parse(bookResponseString);
            bookIndex = rand(0, bookResponseObject.data.length - 1)
            console.log(bookResponseObject.data[bookIndex]);
            
            book = bookResponseObject.data[bookIndex];
            title = book.title;
            author = book.contributors[0].name; //Use first author TODO: use all authors.
            var keynote = striptags(book.keynote);

            bookResponseCallback(null, "How about " + title + " by " + author + '<break time="1s"/>' + keynote);
        });
    }).on('error', function (e) {
        console.log("Communications error: " + e.message);
        bookResponseCallback(new Error(e.message));
    });
}
/**
 * Algorithm to find the 2 high tides for the day, the first of which is smaller and occurs
 * mid-day, the second of which is larger and typically in the evening
 */
function findHighBook(noaaResponseObj) {
    var predictions = noaaResponseObj.predictions;
    var lastPrediction;
    var firstHighBook, secondHighBook, lowBook;
    var firstBookDone = false;

    for (var i = 0; i < predictions.length; i++) {
        var prediction = predictions[i];

        if (!lastPrediction) {
            lastPrediction = prediction;
            continue;
        }

        if (isBookIncreasing(lastPrediction, prediction)) {
            if (!firstBookDone) {
                firstHighBook = prediction;
            } else {
                secondHighBook = prediction;
            }

        } else { // we're decreasing

            if (!firstBookDone && firstHighBook) {
                firstBookDone = true;
            } else if (secondHighBook) {
                break; // we're decreasing after have found 2nd tide. We're done.
            }

            if (firstBookDone) {
                lowBook = prediction;
            }
        }

        lastPrediction = prediction;
    }

    return {
        firstHighBookTime: alexaDateUtil.getFormattedTime(new Date(firstHighBook.t)),
        firstHighBookHeight: getFormattedHeight(firstHighBook.v),
        lowBookTime: alexaDateUtil.getFormattedTime(new Date(lowBook.t)),
        lowBookHeight: getFormattedHeight(lowBook.v),
        secondHighBookTime: alexaDateUtil.getFormattedTime(new Date(secondHighBook.t)),
        secondHighBookHeight: getFormattedHeight(secondHighBook.v)
    }
}

function isBookIncreasing(lastPrediction, currentPrediction) {
    return parseFloat(lastPrediction.v) < parseFloat(currentPrediction.v);
}

/**
 * Formats the height, rounding to the nearest 1/2 foot. e.g.
 * 4.354 -> "four and a half feet".
 */
function getFormattedHeight(height) {
    var isNegative = false;
    if (height < 0) {
        height = Math.abs(height);
        isNegative = true;
    }

    var remainder = height % 1;
    var feet, remainderText;

    if (remainder < 0.25) {
        remainderText = '';
        feet = Math.floor(height);
    } else if (remainder < 0.75) {
        remainderText = " and a half";
        feet = Math.floor(height);
    } else {
        remainderText = '';
        feet = Math.ceil(height);
    }

    if (isNegative) {
        feet *= -1;
    }

    var formattedHeight = feet + remainderText + " feet";
    return formattedHeight;
}

/**
 * Gets the city from the intent, or returns an error
 */
function getCityStationFromIntent(intent, assignDefault) {

    var citySlot = intent.slots.City;
    // slots can be missing, or slots can be provided but with empty value.
    // must test for both.
    if (!citySlot || !citySlot.value) {
        if (!assignDefault) {
            return {
                error: true
            }
        } else {
            // For sample skill, default to Seattle.
            return {
                city: 'seattle',
                station: STATIONS.seattle
            }
        }
    } else {
        // lookup the city. Sample skill uses well known mapping of a few known cities to station id.
        var cityName = citySlot.value;
        if (STATIONS[cityName.toLowerCase()]) {
            return {
                city: cityName,
                station: STATIONS[cityName.toLowerCase()]
            }
        } else {
            return {
                error: true,
                city: cityName
            }
        }
    }
}

function getImprintFromIntent(intent, assignDefault) {

    var imprintSlot = intent.slots.Imprint;
    // slots can be missing, or slots can be provided but with empty value.
    // must test for both.
    if (!imprintSlot || !imprintSlot.value) {
        if (!assignDefault) {
            return {
                error: true
            }
        } else {
            // For sample skill, default to Seattle.
            return {
                imprint: 'AvonRomance',
                error: false
            }
        }
    } else {
        // lookup the city. Sample skill uses well known mapping of a few known cities to station id.
        var imprintName = imprintSlot.value;

        console.log('imprintName:'+imprintName);

        return {
            imprint: imprintName,
            error: false
        }
    }
}

/**
 * Gets the date from the intent, defaulting to today if none provided,
 * or returns an error
 */
function getDateFromIntent(intent) {

    var dateSlot = intent.slots.Date;
    // slots can be missing, or slots can be provided but with empty value.
    // must test for both.
    if (!dateSlot || !dateSlot.value) {
        // default to today
        return {
            displayDate: "Today",
            requestDateParam: "date=today"
        }
    } else {

        var date = new Date(dateSlot.value);

        // format the request date like YYYYMMDD
        var month = (date.getMonth() + 1);
        month = month < 10 ? '0' + month : month;
        var dayOfMonth = date.getDate();
        dayOfMonth = dayOfMonth < 10 ? '0' + dayOfMonth : dayOfMonth;
        var requestDay = "begin_date=" + date.getFullYear() + month + dayOfMonth
            + "&range=24";

        return {
            displayDate: alexaDateUtil.getFormattedDate(date),
            requestDateParam: requestDay
        }
    }
}

function getAllImprintsText() {
    var imprintList = '';
    for (var station in STATIONS) {
        imprintList += station + ", ";
    }

    return imprintList;
}

function getAllStationsText() {
    var stationList = '';
    for (var station in STATIONS) {
        stationList += station + ", ";
    }

    return stationList;
}

// Create the handler that responds to the Alexa Request.
exports.handler = function (event, context) {
    var bookRecommendation = new BookRecommendation();
    bookRecommendation.execute(event, context);
};

